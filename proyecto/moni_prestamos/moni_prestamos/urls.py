"""moni_prestamos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from apps.principal.views import index,listadoPrestamos,solicitarPrestamo,prestamoAprobado,prestamoRechazado,editarPrestamo,eliminarPrestamo

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', index, name = 'index'),

    path('login', LoginView.as_view(template_name='login.html'), name = 'login'),
    path('logout', LogoutView.as_view(template_name='logout.html'), name = 'logout'),

    path('listado_prestamos', login_required(listadoPrestamos), name = 'listado_prestamos'),

    path('solicitar_prestamo', solicitarPrestamo, name='solicitar_prestamo'),
    path('editar_prestamo/<int:id>/', login_required(editarPrestamo), name='editar_prestamo'),
    path('eliminar_prestamo/<int:id>/', login_required(eliminarPrestamo), name='eliminar_prestamo'),

    path('prestamo_aprobado', prestamoAprobado, name='prestamo_aprobado'),
    path('prestamo_rechazado', prestamoRechazado, name='prestamo_rechazado'),
]

handler404 = "apps.principal.views.page_not_found_view"
handler500 = "apps.principal.views.internal_server_error_view"
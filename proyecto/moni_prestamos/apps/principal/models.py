from django.core.exceptions import ValidationError
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.utils.translation import ugettext_lazy as _

GENERO_CHOICES = (
    ('Femenino', 'Femenino'),
    ('Masculino','Masculino'),
    ('Otro','Otro'),
)

ESTADO_CHOICES = (
    ('Aprobado', 'Aprobado'),
    ('Rechazado', 'Rechazado')
)

# Create your models here.

class Prestamo(models.Model):
    id = models.AutoField(primary_key = True)
    dni = models.IntegerField(unique = True, help_text='Sin espacios, puntos o comas. Ej. 14254785', validators=[MaxValueValidator(99999999, "El DNI ingresado NO es válido. Intentá nuevamente."), MinValueValidator(1111111, "El DNI ingresado NO es válido. Intentá nuevamente.")])
    nombre = models.CharField(max_length = 30, validators=[
        RegexValidator(
            regex='^[a-zA-ZÁÉÍÓÚáéíóú ]{3,30}$',
            message='El nombre es inválido. Recordá que debe tener entre 3 y 30 caracteres (mayúsculas y minúsculas).',
            code='invalid_nombre'
        ),
    ])
    apellido = models.CharField(max_length = 30, validators=[
        RegexValidator(
            regex='^[a-zA-ZÁÉÍÓÚáéíóú ]{3,30}$',
            message='El apellido es inválido. Recordá que debe tener entre 3 y 30 caracteres (mayúsculas y minúsculas).',
            code='invalid_apellido'
        ),
    ])
    genero = models.CharField(max_length=21, choices=GENERO_CHOICES, default='Otro')
    email = models.EmailField(max_length = 100, help_text='Ej. ruben@gmail.com', unique = True)
    monto_solicitado = models.BigIntegerField(help_text='Ej. 20100')

    estado = models.CharField(max_length = 20, choices=ESTADO_CHOICES)

    def __str__(self):
        return self.nombre
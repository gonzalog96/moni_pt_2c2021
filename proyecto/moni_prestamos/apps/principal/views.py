from django.shortcuts import render,redirect
from .models import Prestamo
from .forms import PrestamoForm,PrestamoFormAdmin
from .services import prestamo_aprobado

# Create your views here.

# index
def index(request):
    return render(request, 'index.html')

# listado de préstamos.
def listadoPrestamos(request):
    prestamos = Prestamo.objects.all() # select * from Prestamo

    contexto = {
        'prestamos' : prestamos
    }

    return render(request, 'listado_prestamos.html', contexto)

# alta de préstamos.
def solicitarPrestamo(request):
    if request.method == 'GET':
        form = PrestamoForm()
        contexto = {
            'form' : form
        }
    else:
        form = PrestamoForm(request.POST)
        contexto = {
            'form' : form
        }

        # verificamos si el formulario es válido.
        if form.is_valid():
            obj = form.save(commit=False)
            if prestamo_aprobado(request.POST.get('dni')):
                obj.estado = "Aprobado"
            else:
                obj.estado = "Rechazado"
            obj.save()
            return redirect('prestamo_aprobado' if obj.estado == "Aprobado" else 'prestamo_rechazado')

    return render(request, 'solicitud_prestamo.html', contexto)

# edición de préstamos.
def editarPrestamo(request, id):
    prestamo = Prestamo.objects.get(id = id)

    if request.method == 'GET':
        form = PrestamoFormAdmin(instance = prestamo)
        contexto = {
            'form' : form
        }
    else:
        form = PrestamoFormAdmin(request.POST, instance = prestamo)
        contexto = {
            'form' : form
        }

        if (form.is_valid()):
            form.save()
            return redirect('listado_prestamos')

    return render(request, 'edicion_prestamo.html', contexto)

# eliminación de préstamos.
def eliminarPrestamo(request, id):
    prestamo = Prestamo.objects.get(id = id)
    prestamo.delete()
    return redirect('listado_prestamos')


def prestamoAprobado(request):
    return render(request, 'prestamo_aceptado.html')

def prestamoRechazado(request):
    return render(request, 'prestamo_rechazado.html')

def page_not_found_view(request, exception):
    return render(request, '404.html', status=404)

def internal_server_error_view(request):
    return render(request, '500.html', status=500)
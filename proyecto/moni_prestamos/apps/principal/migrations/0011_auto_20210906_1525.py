# Generated by Django 3.1.2 on 2021-09-06 18:25

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0010_auto_20210905_2200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prestamo',
            name='apellido',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(code='invalid_apellido', message='El apellido es inválido. Recordá que debe tener entre 3 y 30 caracteres (mayúsculas y minúsculas).', regex='^[a-zA-ZÁÉÍÓÚáéíóú ]{3,30}$')]),
        ),
        migrations.AlterField(
            model_name='prestamo',
            name='monto_solicitado',
            field=models.BigIntegerField(help_text='Ej. 20100'),
        ),
        migrations.AlterField(
            model_name='prestamo',
            name='nombre',
            field=models.CharField(max_length=30, validators=[django.core.validators.RegexValidator(code='invalid_nombre', message='El nombre es inválido. Recordá que debe tener entre 3 y 30 caracteres (mayúsculas y minúsculas).', regex='^[a-zA-ZÁÉÍÓÚáéíóú ]{3,30}$')]),
        ),
    ]

from django.utils.translation import gettext_lazy as _
from django import forms
from .models import Prestamo

class PrestamoForm(forms.ModelForm):
    class Meta:
        model = Prestamo
        #fields = '__all__'
        fields = ('dni', 'nombre', 'apellido', 'genero', 'email', 'monto_solicitado',)

        error_messages = {
            'nombre': {
              'required' : _('este campo es requerido.'),  
            },
            'apellido': {
                'required' : _('este campo es requerido.'),
            },
            'dni': {
                'unique': _("el DNI ingresado ya efectuó una solicitud de préstamo."),
            },
            'email': {
                'unique': _("el email ingresado está en uso."),
            },
        }


class PrestamoFormAdmin(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields = '__all__'
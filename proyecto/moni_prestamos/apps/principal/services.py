import requests

randomuser_api = 'https://randomuser.me/api'
api_moni_scoring = 'https://api.moni.com.ar/api/v4/scoring/pre-score/'
headers = {'credential' : 'ZGpzOTAzaWZuc2Zpb25kZnNubm5u'}

def generate_request(url, headers={}, params={}):
    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        return response.json()

def prestamo_aprobado(dni, params={}):
    response = generate_request(api_moni_scoring + str(dni), headers=headers)

    if response:
        estado = response.get('status')
        return estado == 'approve'
    
    return ''
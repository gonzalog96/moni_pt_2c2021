
# Proyecto 'PréstamosOnline' ![](https://i.ibb.co/pj6TGZS/imageedit-4-2006011538.png)

## 🛈¿De qué se trata?

Es una *web app* que **permite a los usuarios solicitar préstamos.** La aprobación o rechazo se valida a través de una [API externa](https://api.moni.com.ar/api/v4/scoring/pre-score/) (por parte de la plataforma *MONI*).

Por otra parte, la aplicación contempla una **sección de administración** para que los moderadores puedan **editar o eliminar las solicitudes de préstamos.**

## 🛈¿Cómo se ejecuta?
Desde la terminal o línea de comandos, se debe ejecutar:

    python manage.py runserver

Tener en cuenta que, por defecto, el modo DEBUG está deshabilitado. Para activarlo, se debe cambiar la línea del settings.py:

    DEBUG = False

Por:

    DEBUG = True

## 📟¿Qué tecnologías se utilizaron?
- Django v3.1.2.
- Bootstrap.
- SQLite.

## ⌛¿Cuánto fue el tiempo estimado de desarrollo?

Se contabilizaron, aproximadamente, **35 horas** para desarrollar el proyecto. En este sentido:

 - La mayor parte de las horas fueron demandadas para **investigación y prueba** de los modelos de Django. Personalmente, no tenía experiencia en el uso de este *framework*, aunque sí conocimientos iniciales sobre algoritmos con Python.

## 📑¿Cuáles fueron los principales desafíos?

 - Entender **cómo funciona Django**, específicamente el patrón **MVT** (*Model - View - Template*).
 - Comprender **cómo utilizar la herencia en los *templates.***
 - Definir las **validaciones de los modelos**, utilizando *RegEx.*
 - Estructurar los *templates* con ***Bootstrap* para mostrar adecuadamente la información.**
 - Articular la respuesta de la **API con la propia validación del formulario.**
 - Cargar el contenido de la carpeta **static**.

## 🔄¿Se piensa mejorar/agregar alguna *feature*?

**Sí**, pienso que sería interesante extender la *app* y agregar:

 - **ABM para los clientes**, asociando cada uno de ellos con cero o más préstamos.
 - Utilizar una **BD** como **PostgreSQL**.
 - ***Class-based views*** para evitar código repetido (*boilerplate*).
 - ***Modals* con *Bootstrap*** para mejorar la usabilidad del sitio.
 - **Confirmación de préstamos vía e-mail:** que el sistema notifique al usuario si su préstamo fue o no aprobado.
 - *Deployear* la app en **Heroku** o alguna otra plataforma *Cloud*.

## 🚩Comentarios finales

Fue una experiencia interesante, motivadora y desafiante. Viniendo del desarrollo en *Java*, articular los conocimientos de POO con Django no fue tan difícil, aunque sí aprender su sintaxis tan particular.

Definitivamente utilizaría Django para armar proyectos personales o profesionales: su robustez, dinamismo y flexibilidad para el desarrollo es muy buena.

## 📨Contacto
 🧍    Gonzalo Godoy
📧 godoyg1996@gmail.com